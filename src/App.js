import React, { Component } from 'react';
import AddTaskForm from './components/AddTaskForm/AddTaskForm';
import Task from "./components/Task/Task"

import './App.css';

class App extends Component {
  state = {
    tasks: [
      {title: 'Buy milk', id: '1545288342874', complete: false},
      {title: 'Walk with dog', id: '1545288342875', complete: false},
      {title: 'Do Homework', id: '1545288342876', complete: false},
      {title: 'Play DOKA2', id: '1545288342877', complete: true}
    ],
    inputValue: ""
  };

  removeTask = index => {
    const tasks = [...this.state.tasks];
    tasks.splice(index, 1);

    this.setState({tasks});
  };

  changeValue = (event) => {
    const inputValue = event.target.value;
    this.setState({inputValue});
  };

  addTask = () => {
    const tasks = [...this.state.tasks];
    let inputValue = (' ' + this.state.inputValue).slice(1);
    const task = {title: inputValue, id: Date.now().toString(), complete: false};
    inputValue = '';
    tasks.push(task);
    this.setState({tasks, inputValue});
  };

  changeTaskStatus = index => {
    const tasks = [...this.state.tasks];
    const task = tasks[index];
    task.complete = !task.complete;
    this.setState(tasks);
  };

  render() {
    const tasks = this.state.tasks.map((task, index) => (
      <Task
        key={task.id}
        title={task.title}
        remove={() => this.removeTask(index)}
        onChange={() => this.changeTaskStatus(index)}
        complete={task.complete}
      >
      </Task>
    ));
    return (
      <div className="App">
        <AddTaskForm
          value={this.state.inputValue}
          onChange={event => this.changeValue(event)}
          onClick={() => this.addTask()}
        />
        {tasks}
      </div>
    );
  }
}

export default App;

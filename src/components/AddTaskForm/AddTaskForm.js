import React from "react"

import './AddTaskForm.sass';

const AddTaskForm = props => (
  <div className="add-task-form">
    <input type="text" value={props.value} onChange={props.onChange} placeholder="Enter new task"/>
    <button type="button" onClick={props.onClick}>Add task</button>
  </div>
);

export default AddTaskForm;
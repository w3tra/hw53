import React from "react"

import './Task.sass';

const Task = props => (
  <div className={"task" + (props.complete ? ' complete' : '') }>
    <p className="task-title">{props.title}</p>
    <div className="task-actions">
      <button onClick={props.remove}>Delete</button>
      <label className="checkbox-container">
        <input type="checkbox" onChange={props.onChange} checked={props.complete}/>
        <span className="checkmark"></span>
      </label>
    </div>
  </div>
);

export default Task;